#!/bin/bash
set -xe

BUILD_DIR=workdir

# From https://stackoverflow.com/a/48808214
args=("$@")
for ((i=0; i<"${#args[@]}"; ++i)); do
    case ${args[i]} in
        -b) BUILD_DIR=${args[i+1]}; unset args[i]; unset args[i+1]; break;;
    esac
done

[ -d build ] || git clone -b halium-12 https://gitlab.com/ubports/community-ports/halium-generic-adaptation-build-tools build

source deviceinfo

mkdir -p "$BUILD_DIR"/downloads
cd "$BUILD_DIR"/downloads

KERNEL_DIR="$(basename "${deviceinfo_kernel_source}")"
KERNEL_DIR="${KERNEL_DIR%.*}"
MODULES_DIR="$(basename "${deviceinfo_kernel_modules_source}")"
MODULES_DIR="${MODULES_DIR%.*}"

[ -d "$MODULES_DIR" ] || git clone -b "${deviceinfo_kernel_modules_source_branch}" \
    "${deviceinfo_kernel_modules_source}" "$MODULES_DIR"

# Fix relative path includes in out-of-tree modules
ln -sfT "${KERNEL_DIR}" kernel-5.10
ln -sfT "$MODULES_DIR"/vendor vendor

cd -

./build/build.sh "${args[@]}" -b "$BUILD_DIR"
